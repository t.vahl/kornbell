import { LocalStorage } from "node-localstorage";
import TelegramBot, { Chat, Contact, Message, MessageType, Metadata, SendMessageOptions } from "node-telegram-bot-api";
import { BellMessenger } from "../../src/bellMessenger/bellMessenger";
import { MessengerStorage } from "../../src/bellMessenger/messengerStorage";
import { TelegramBellMessenger } from "../../src/bellMessenger/telegramBellMessenger";

const immediate = jasmine.objectContaining<SendMessageOptions>({ disable_notification: false });
const whenOnline = jasmine.objectContaining<SendMessageOptions>({ disable_notification: true });
type TextListener = (msg: TelegramBot.Message, match: RegExpExecArray | null) => void;

let ticketListener: TextListener;
let verifyListener: TextListener;
let disableListener: TextListener;
let enableListener: TextListener;
function fakeOnTextFor(spy: jasmine.SpyObj<TelegramBot>) {
    return (regexp: RegExp, callback: TextListener): TelegramBot => {
        if (regexp === TelegramBellMessenger.GET_TICKET_REGEX) {
            ticketListener = callback;
        } else if (regexp === TelegramBellMessenger.VERIFY_REGEX) {
            verifyListener = callback;
        } else if (regexp === TelegramBellMessenger.DISABLE_REGEX) {
            disableListener = callback;
        } else if (regexp === TelegramBellMessenger.ENABLE_REGEX) {
            enableListener = callback;
        }
        return spy;
    }
}
const ticketMatch = TelegramBellMessenger.GET_TICKET_REGEX.exec(TelegramBellMessenger.GET_TICKET_COMMAND);

function createBotSpy() {
    return jasmine.createSpyObj<TelegramBot>("TelegramBot", ["sendMessage", "onText"]);
}

function creatStorageSpy() {
    return jasmine.createSpyObj<MessengerStorage>("MessengerStorage", [
        "getChatIds", "getAdminChatId", "addChatId",
        "addTicket", "getTickets", "removeTicket"
    ]);
}

function replyTo(messageId: number) {
    return jasmine.objectContaining<SendMessageOptions>({
        reply_to_message_id: messageId
    });
}

function aMessageWith(options: Partial<Message>): Message {
    const message: Message = {
        "message_id": 3248987,
        date: 1618074079,
        chat: aPrivateChatWith({})
    };
    return Object.assign(message, options);
}

function aPrivateChatWith(options: Partial<Chat>): Chat {
    const chat: Chat = {
        id: 9083712,
        type: "private"
    };
    return Object.assign(chat, options);
}

function sendDisableMessage() {
    const messageText = "/disable";
    const disableMessage = aMessageWith({ text: messageText });
    disableListener(disableMessage, TelegramBellMessenger.DISABLE_REGEX.exec(messageText));
}

function sendEnableMessage() {
    const messageText = "/enable";
    const enableMessage = aMessageWith({ text: messageText });
    enableListener(enableMessage, TelegramBellMessenger.ENABLE_REGEX.exec(messageText));
}

describe("TelegramBellMessenger", () => {
    let botSpy: jasmine.SpyObj<TelegramBot>;
    let storage: jasmine.SpyObj<MessengerStorage>;
    let messenger: BellMessenger;
    let chats: number[];
    let adminChatId: number;

    beforeEach(() => {
        chats = [12312312, 1123123];
        adminChatId = 66666666;

        botSpy = createBotSpy();
        botSpy.onText.and.callFake(fakeOnTextFor(botSpy));

        storage = creatStorageSpy();
        storage.getChatIds.and.returnValue(chats);
        storage.getAdminChatId.and.returnValue(adminChatId);

        messenger = new TelegramBellMessenger(botSpy, storage);
    });

    it("sends immediate message to all chats when cat is waiting", () => {
        messenger.sendCatWaiting();

        expect(botSpy.sendMessage).toHaveBeenCalledWith(chats[0], "Cat waiting", immediate);
        expect(botSpy.sendMessage).toHaveBeenCalledWith(chats[1], "Cat waiting", immediate);
    });

    it("sends when-online-message to all chats when cat is gone", () => {
        messenger.sendCatGone();

        expect(botSpy.sendMessage).toHaveBeenCalledWith(chats[0], "Cat gone", whenOnline);
        expect(botSpy.sendMessage).toHaveBeenCalledWith(chats[1], "Cat gone", whenOnline);
    });

    it("sends when-online-message to all chats when cat is inside", () => {
        messenger.sendCatInside();

        expect(botSpy.sendMessage).toHaveBeenCalledWith(chats[0], "Cat is inside", whenOnline);
        expect(botSpy.sendMessage).toHaveBeenCalledWith(chats[1], "Cat is inside", whenOnline);
    });

    it("does not send messages when it was disabled", () => {
        sendDisableMessage();
        botSpy.sendMessage.calls.reset();

        messenger.sendCatWaiting();

        expect(botSpy.sendMessage).not.toHaveBeenCalled();
    });

    it("sends notification to all users when it is disabled", () => {
        sendDisableMessage();

        expect(botSpy.sendMessage).toHaveBeenCalledWith(chats[0],
            "KornBell is now disabled. No one will receive any more messages until re-enabled using `/enable`.",
            immediate);
    });

    it("sends messages again when it was re-enabled", () => {
        sendDisableMessage();

        sendEnableMessage();
        messenger.sendCatWaiting();

        expect(botSpy.sendMessage).toHaveBeenCalledWith(chats[0], "Cat waiting", immediate);
    });

    it("sends notification to all users when it is re-enabled", () => {
        sendDisableMessage();
        botSpy.sendMessage.calls.reset();

        sendEnableMessage();

        expect(botSpy.sendMessage).toHaveBeenCalledWith(chats[0],
            "KornBell is enabled again.",
            immediate);
    });

    it("adds and answers with a new ticket when the admin asks for one", () => {
        const message = aMessageWith({
            text: TelegramBellMessenger.GET_TICKET_COMMAND,
            chat: aPrivateChatWith({ id: adminChatId })
        });

        ticketListener(message, ticketMatch);

        const ticketId = storage.addTicket.calls.mostRecent().args[0];
        expect(botSpy.sendMessage).toHaveBeenCalledOnceWith(
            adminChatId, `New ticket is ${ticketId}`, replyTo(message.message_id)
        );
    });

    it("does nothing when a non admin asks for a new ticket", () => {
        const message = aMessageWith({
            text: TelegramBellMessenger.GET_TICKET_COMMAND,
            chat: aPrivateChatWith({ id: adminChatId + 1 })
        });

        ticketListener(message, ticketMatch);

        expect(storage.addTicket).not.toHaveBeenCalled();
        expect(botSpy.sendMessage).not.toHaveBeenCalled();
    });

    it("generates a different ticket each time", () => {
        const message = aMessageWith({
            text: TelegramBellMessenger.GET_TICKET_COMMAND,
            chat: aPrivateChatWith({ id: adminChatId })
        });
        const secondMessage = Object.assign({}, message);
        secondMessage.message_id = 324234;

        ticketListener(message, ticketMatch);
        ticketListener(secondMessage, ticketMatch);

        const ticketIds = storage.addTicket.calls.all()
            .map(call => call.args[0]);
        expect(ticketIds[0]).not.toEqual(ticketIds[1]);
    });

    it("adds a chat if it verifies using a ticket", () => {
        const expectedChatId = 19120380921;
        const ticket = "809203";
        const message = aMessageWith({
            text: `/verify ${ticket}`,
            chat: aPrivateChatWith({ id: expectedChatId })
        });
        storage.getTickets.and.returnValue([ticket]);

        verifyListener(message, TelegramBellMessenger.VERIFY_REGEX.exec(message.text!));

        expect(storage.addChatId).toHaveBeenCalledWith(expectedChatId);
    });

    it("answers 'verified' if a chat is verified using a ticket", () => {
        const expectedChatId = 19120380921;
        const ticket = "809203";
        const message = aMessageWith({
            text: `/verify ${ticket}`,
            chat: aPrivateChatWith({ id: expectedChatId })
        });
        storage.getTickets.and.returnValue([ticket]);

        verifyListener(message, TelegramBellMessenger.VERIFY_REGEX.exec(message.text!));

        expect(botSpy.sendMessage).toHaveBeenCalledWith(
            expectedChatId, "Verfied", replyTo(message.message_id)
        );
    });

    it("removes ticket when used", () => {
        const expectedChatId = 19120380921;
        const ticket = "809203";
        const message = aMessageWith({
            text: `/verify ${ticket}`,
            chat: aPrivateChatWith({ id: expectedChatId })
        });
        storage.getTickets.and.returnValue([ticket]);

        verifyListener(message, TelegramBellMessenger.VERIFY_REGEX.exec(message.text!));

        expect(storage.removeTicket).toHaveBeenCalledWith(ticket);
    })

    it("does nothing if verification is attempted with an invalid ticket", () => {
        const expectedChatId = 19120380921;
        const ticket = "809203";
        const message = aMessageWith({
            text: `/verify ${ticket}`,
            chat: aPrivateChatWith({ id: expectedChatId })
        });
        storage.getTickets.and.returnValue([]);

        verifyListener(message, TelegramBellMessenger.VERIFY_REGEX.exec(message.text!));

        expect(storage.addChatId).not.toHaveBeenCalledWith(expectedChatId);
    });

    it("does nothing if verification is attempted without a ticket", () => {
        const expectedChatId = 19120380921;
        const ticket = "";
        const message = aMessageWith({
            text: `/verify ${ticket}`,
            chat: aPrivateChatWith({ id: expectedChatId })
        });
        storage.getTickets.and.returnValue([]);

        verifyListener(message, TelegramBellMessenger.VERIFY_REGEX.exec(message.text!));

        expect(storage.addChatId).not.toHaveBeenCalledWith(expectedChatId);
    });
});

