import { LocalStorage } from "node-localstorage";
import { LocalMessengerStorage } from "../../src/bellMessenger/localMessengerStorage";

type SpyObj<C> = jasmine.SpyObj<C>;

describe("LocalMessengerStorage", () => {
    let localStorage: SpyObj<LocalStorage>;
    let messengerStorage: LocalMessengerStorage;

    beforeEach(() => {
        localStorage = jasmine.createSpyObj("LocalStorage", ["getItem", "setItem"]);
        messengerStorage = new LocalMessengerStorage(localStorage);

        localStorage.getItem.and.returnValue(null);
    });

    it("reads the admin chat id", () => {
        const expectedAdminChatId = 324923423;
        localStorage.getItem.and.returnValue(`${expectedAdminChatId}`);
        messengerStorage = new LocalMessengerStorage(localStorage);

        const actualAdminChatId = messengerStorage.getAdminChatId();

        expect(actualAdminChatId).toBe(expectedAdminChatId);
    });

    it("reads admin chat id -1 if no admin chat id is configured", () => {
        const expectedAdminChatId = -1;
        localStorage.getItem.and.returnValue(null);
        messengerStorage = new LocalMessengerStorage(localStorage);

        const actualAdminChatId = messengerStorage.getAdminChatId();

        expect(actualAdminChatId).toBe(expectedAdminChatId);
    });

    it("reads admin chat id -1 if the configured admin chat id is invalid", () => {
        const expectedAdminChatId = -1;
        localStorage.getItem.and.returnValue(`ff`);
        messengerStorage = new LocalMessengerStorage(localStorage);

        const actualAdminChatId = messengerStorage.getAdminChatId();

        expect(actualAdminChatId).toBe(expectedAdminChatId);
    });

    it("updates the tickets when new ticket is added", () => {
        const newTicket = "2698684";
        const existingTickets = ["5909083", "92034"];
        const expectedTickets = [...existingTickets, newTicket];
        localStorage.getItem.and.returnValue(JSON.stringify(existingTickets));

        messengerStorage.addTicket(newTicket);

        expect(localStorage.setItem).toHaveBeenCalledOnceWith(
            "tickets",
            JSON.stringify(expectedTickets)
        );
    });

    it("removes the ticket", () => {
        const ticketToRemove = "92034";
        const expectedTickets = ["3289423"]
        const existingTickets = [ticketToRemove, ...expectedTickets];
        localStorage.getItem.and.returnValue(JSON.stringify(existingTickets));

        messengerStorage.removeTicket(ticketToRemove);

        expect(localStorage.setItem).toHaveBeenCalledOnceWith(
            "tickets",
            JSON.stringify(expectedTickets)
        );
    });

    it("reads the tickets", () => {
        const expectedTickets = ["123123", "9823409238"];
        localStorage.getItem.and.returnValue(JSON.stringify(expectedTickets));

        const actualTickets = messengerStorage.getTickets();

        expect(actualTickets).toEqual(expectedTickets);
    });

    it("updates the chats when new chat is added", () => {
        const newChat = 2698684;
        const existingChats = [5909083, 92034];
        const expectedChats = [...existingChats, newChat];
        localStorage.getItem.and.returnValue(JSON.stringify(existingChats));

        messengerStorage.addChatId(newChat);

        expect(localStorage.setItem).toHaveBeenCalledOnceWith(
            "chats",
            JSON.stringify(expectedChats)
        );
    });

    it("reads the chats", () => {
        const expectedChats = [123123, 9823409238];
        localStorage.getItem.and.returnValue(JSON.stringify(expectedChats));

        const actualTickets = messengerStorage.getChatIds();

        expect(actualTickets).toEqual(expectedChats);
    });
});
