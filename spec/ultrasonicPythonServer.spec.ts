import { ChildProcess, spawn } from "child_process";
import { Readable } from "stream";
import { Spawner, UltrasonicPythonServer } from "../src/ultrasonicPythonServer";

type DataListener = (chunk: any) => void;

describe("UltrasonicPythonServer", () => {
    let server: UltrasonicPythonServer;
    let spawnSpy: jasmine.Spy<Spawner>;
    let childProcessSpy: jasmine.SpyObj<ChildProcess>;
    let stdOutCallback: DataListener;

    beforeEach(() => {
        let stdOutSpy = jasmine.createSpyObj<Readable>("StdOut", ["on"]);
        childProcessSpy = jasmine.createSpyObj<ChildProcess>("ChildProcessSpy",
            ["on"],
            {
                stdout: stdOutSpy,
                stderr: null
            });
        stdOutSpy.on.and.callFake(((event: "data", listener: DataListener) => {
            if (event === "data") stdOutCallback = listener;
        }) as any);
        spawnSpy = jasmine.createSpy("Spawner");
        spawnSpy.and.returnValue(childProcessSpy);
        server = new UltrasonicPythonServer(spawnSpy);
    });

    it("launches the child process when created", () => {
        expect(spawnSpy).toHaveBeenCalledOnceWith("python3", jasmine.arrayWithExactContents(["ultrasonic.py"]));
    });

    it("parses value", () => {
        stdOutCallback("23.5\n");

        expect(server.getLast()).toBe(23.5);
    });
});
