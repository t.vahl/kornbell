import { CatStateListener, CatStateManager } from "../src/catStateManager";
import { UltraSonicThresholdEmitter, UltrasonicThresholdListener } from "../src/ultrasonicThresholdEmitter";

describe("CatStateManager", () => {
    let stateManager: CatStateManager;
    let emitterSpy: jasmine.SpyObj<UltraSonicThresholdEmitter>;
    let listener: UltrasonicThresholdListener | undefined;
    let catStateListenerSpy: jasmine.SpyObj<CatStateListener>;
    const minimumWaitingMillis = 10 * 1000;

    beforeEach(() => {
        emitterSpy = jasmine.createSpyObj("EmitterSpy", ["addListener"]);
        emitterSpy.addListener.and.callFake((l) => {
            listener = l;
        });

        catStateListenerSpy = jasmine.createSpyObj("CatStateListenerSpy", ["catWaiting", "catGone"]);

        stateManager = new CatStateManager(emitterSpy, minimumWaitingMillis);
        stateManager.addListener(catStateListenerSpy);
    });

    it("fires 'cat waiting' when threshold is undercut for longer than the minimum waiting time", () => {
        jasmine.clock().install();
        listener?.belowThreshold();

        jasmine.clock().tick(minimumWaitingMillis - 1000);
        expect(catStateListenerSpy.catWaiting).not.toHaveBeenCalled();

        jasmine.clock().tick(2000);
        expect(catStateListenerSpy.catWaiting).toHaveBeenCalledOnceWith();
        jasmine.clock().uninstall();
    });

    it("fires 'cat gone' when exceeding threshold", () => {
        listener?.aboveThreshold();

        expect(catStateListenerSpy.catGone).toHaveBeenCalledOnceWith();
    });

    it("does not fire 'cat waiting' when having exceed the threshold within the 10 seconds", () => {
        jasmine.clock().install();
        listener?.aboveThreshold();

        jasmine.clock().tick(7000);
        listener?.belowThreshold();

        jasmine.clock().tick(2000);
        listener?.aboveThreshold();

        jasmine.clock().tick(2000);
        expect(catStateListenerSpy.catWaiting).not.toHaveBeenCalled();
        jasmine.clock().uninstall();
    });

    it("does not send 'cat gone' when waiting is canceled", () => {
        jasmine.clock().install();
        listener?.belowThreshold();
        jasmine.clock().tick(7000);

        listener?.aboveThreshold();
        jasmine.clock().tick(2000);

        listener?.belowThreshold();
        jasmine.clock().tick(2000);

        expect(catStateListenerSpy.catGone).not.toHaveBeenCalled();
        jasmine.clock().uninstall();
    });

    it("does not fire 'cat gone' on setup", () => {
        expect(catStateListenerSpy.catGone).not.toHaveBeenCalled();
    });

    it("does not fire 'cat waiting' on setup", () => {
        expect(catStateListenerSpy.catWaiting).not.toHaveBeenCalled();
    });
});
