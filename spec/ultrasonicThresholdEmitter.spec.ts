import { createSensor } from "mmm-usonic";
import { UltrasonicSensor, UltraSonicThresholdEmitter, UltrasonicThresholdListener } from "../src/ultrasonicThresholdEmitter";


describe("UltraSonicThresholdEmitter", () => {
    let sensorSpy: jasmine.SpyObj<UltrasonicSensor>;
    let listenerSpy: jasmine.SpyObj<UltrasonicThresholdListener>;
    let emitter: UltraSonicThresholdEmitter;
    let thresholdCms: number;
    let intervalMillis: number;

    function moveBelow() {
        sensorSpy.measureCm.and.returnValue(thresholdCms + 1);
        jasmine.clock().tick(intervalMillis);
        sensorSpy.measureCm.and.returnValue(thresholdCms - 1);
        jasmine.clock().tick(intervalMillis);
    }

    beforeEach(() => {
        jasmine.clock().install();

        thresholdCms = 50;
        intervalMillis = 1000;

        sensorSpy = jasmine.createSpyObj<UltrasonicSensor>("DistanceSpy", {
            measureCm: 0
        });
        listenerSpy = jasmine.createSpyObj<UltrasonicThresholdListener>("ListenerSpy", ["aboveThreshold", "belowThreshold"]);

        emitter = new UltraSonicThresholdEmitter(sensorSpy, thresholdCms, intervalMillis);
        emitter.addListener(listenerSpy);
    });

    afterEach(() => {
        jasmine.clock().uninstall();
    });

    it("measures once per interval", () => {
        sensorSpy.measureCm.calls.reset();
        sensorSpy.measureCm.and.returnValue(100);

        expect(sensorSpy.measureCm).not.toHaveBeenCalled();
        jasmine.clock().tick(intervalMillis);

        expect(sensorSpy.measureCm).toHaveBeenCalledOnceWith();
    });

    it("does change listener method to belowThreshold when value drops below threshold", () => {
        sensorSpy.measureCm.and.returnValue(thresholdCms + 1);
        jasmine.clock().tick(intervalMillis);

        expect(listenerSpy.aboveThreshold).toHaveBeenCalledTimes(1);
        expect(listenerSpy.belowThreshold).toHaveBeenCalledTimes(0);

        sensorSpy.measureCm.and.returnValue(thresholdCms - 1);
        jasmine.clock().tick(intervalMillis);

        expect(listenerSpy.aboveThreshold).toHaveBeenCalledTimes(1);
        expect(listenerSpy.belowThreshold).toHaveBeenCalledTimes(1);
    });

    it("does change listener method to above threshold when value raises above threshold", () => {
        sensorSpy.measureCm.and.returnValue(thresholdCms - 1);
        jasmine.clock().tick(intervalMillis);

        expect(listenerSpy.aboveThreshold).toHaveBeenCalledTimes(0);
        expect(listenerSpy.belowThreshold).toHaveBeenCalledTimes(0);

        sensorSpy.measureCm.and.returnValue(thresholdCms + 1);
        jasmine.clock().tick(intervalMillis);

        expect(listenerSpy.aboveThreshold).toHaveBeenCalledTimes(1);
        expect(listenerSpy.belowThreshold).toHaveBeenCalledTimes(0);
    });

    it("does not trigger aboveThreshold again, when value stays above threshold", () => {
        sensorSpy.measureCm.and.returnValue(thresholdCms + 1);
        jasmine.clock().tick(intervalMillis);

        expect(listenerSpy.aboveThreshold).toHaveBeenCalledTimes(1);

        jasmine.clock().tick(intervalMillis);

        expect(listenerSpy.aboveThreshold).toHaveBeenCalledTimes(1);
    });

    it("does not trigger belowThreshold again, when value stays below threshold", () => {
        moveBelow();

        expect(listenerSpy.belowThreshold).toHaveBeenCalledTimes(1);

        jasmine.clock().tick(intervalMillis);

        expect(listenerSpy.belowThreshold).toHaveBeenCalledTimes(1);
    });

    it("does not call the listener immediately when added", () => {
        sensorSpy.measureCm.and.returnValue(thresholdCms - 1);
        let anotherListener = jasmine.createSpyObj<UltrasonicThresholdListener>("AnotherListener", ["aboveThreshold", "belowThreshold"]);

        emitter.addListener(anotherListener);

        expect(anotherListener.belowThreshold).not.toHaveBeenCalled();
    });
});
