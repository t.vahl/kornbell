import { ChildProcess } from "child_process";

export class UltrasonicPythonServer {
    private child: ChildProcess;
    private buffer = "";
    private lastValue = -1;

    public constructor(private spawner: Spawner) {
        this.child = this.createProcess();
        this.attachProcessListeners();
    }

    private createProcess() {
        return this.spawner("python3", ["ultrasonic.py"]);
    }

    private attachProcessListeners() {
        this.child.stdout?.on("data", data => this.onNewData(data));
        this.child.stdout?.on("error", console.error);
        this.child.on("error", console.error);
    }

    private onNewData(data: any) {
        this.buffer += new String(data);
        this.processBuffer();
    }

    private processBuffer() {
        let lineBreakIndex = this.buffer.indexOf("\n");
        while (lineBreakIndex > -1) {
            const prefix = this.buffer.substring(0, lineBreakIndex + "\n".length);
            this.lastValue = parseFloat(prefix.trim());
            this.buffer = this.buffer.substring(lineBreakIndex + "\n".length);
            lineBreakIndex = this.buffer.indexOf("\n");
        }
    }

    public getLast() {
        return this.lastValue;
    }

    public getReady() {
        return new Promise<void>(res => {
            let waitTimer: () => void;
            waitTimer = () => {
                if (this.getLast() === -1) {
                    setTimeout(waitTimer, 100)
                } else {
                    res();
                }
            }
            setTimeout(waitTimer, 100);
        });
    }
}

export type Spawner = (command: string, args: readonly string[]) => ChildProcess;
