declare module 'mmm-usonic' {
    declare function init(callback: (error: any) => void): void;
    declare function createSensor(echoPin: number, triggerPin: number, timeout?: number):
        () => number;
}
