
export class UltraSonicThresholdEmitter {
    private interval: NodeJS.Timeout;
    private listeners: UltrasonicThresholdListener[] = [];
    private wasAbove: boolean;

    public constructor(
        private sensor: UltrasonicSensor,
        private thresholdCms: number,
        private intervalMillis: number = 1000
    ) {
        this.wasAbove = this.isAbove();
        this.interval = setInterval(() => this.checkUpdate(), this.intervalMillis);
    }

    private isAbove() {
        return this.sensor.measureCm() > this.thresholdCms;
    }

    private checkUpdate() {
        const isAbove = this.isAbove();
        if (isAbove !== this.wasAbove) {
            this.notifyAll(isAbove);
            this.wasAbove = isAbove;
        }
    }

    private notifyAll(above: boolean) {
        const method = this.getListenerMethodFor(above);
        this.listeners.forEach(listener => listener[method]());
    }

    private getListenerMethodFor(above: boolean) {
        return above ? "aboveThreshold" : "belowThreshold";;
    }

    public addListener(listener: UltrasonicThresholdListener) {
        this.listeners.push(listener);
    }
}

export interface UltrasonicSensor {
    measureCm(): number;
}

export interface UltrasonicThresholdListener {
    aboveThreshold(): void;
    belowThreshold(): void
}
