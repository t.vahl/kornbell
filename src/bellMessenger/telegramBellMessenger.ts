import TelegramBot, { Message } from "node-telegram-bot-api";
import { BellMessenger } from "./bellMessenger";
import { MessengerStorage } from "./messengerStorage";

type ContactMessage = Message & { contact: { phone_number: string } };

export class TelegramBellMessenger implements BellMessenger {
    public static readonly TICKET_LENGTH = 6;
    public static readonly GET_TICKET_COMMAND = "/getTicket";
    public static readonly GET_TICKET_REGEX = /\/getTicket/;
    public static readonly VERIFY_COMMAND = "/verify";
    public static readonly VERIFY_REGEX = new RegExp(`/verify (\\d{${TelegramBellMessenger.TICKET_LENGTH}})`);
    public static readonly DISABLE_COMMAND = "/disable";
    public static readonly DISABLE_REGEX = /\/disable/;
    public static readonly ENABLE_COMMAND = "/enable";
    public static readonly ENABLE_REGEX = /\/enable/;

    private disabled = false;

    public constructor(
        private bot: TelegramBot,
        private storage: MessengerStorage
    ) {
        this.bot.onText(TelegramBellMessenger.GET_TICKET_REGEX, message => this.onTicketMessage(message));
        this.bot.onText(TelegramBellMessenger.VERIFY_REGEX, (message, match) => this.onVerifyMessage(message, match));
        this.bot.onText(TelegramBellMessenger.DISABLE_REGEX, () => this.onDisableMessage());
        this.bot.onText(TelegramBellMessenger.ENABLE_REGEX, () => this.onEnableMessage());
    }

    private onTicketMessage(message: Message): void {
        if (!this.isMessageFromAdmin(message)) {
            return;
        }

        const originChat = message.chat.id;
        const ticket = this.createTicket();
        this.storage.addTicket(ticket);
        this.bot.sendMessage(originChat, `New ticket is ${ticket}`, {
            reply_to_message_id: message.message_id
        });
    }

    private createTicket(): string {
        let ticket = "";
        for (let currentLength = 0; currentLength < 6; currentLength++) {
            ticket += Math.floor(Math.random() * 9)
        }
        return ticket;
    }

    private isMessageFromAdmin(message: Message) {
        return message.chat.id === this.storage.getAdminChatId();
    }

    private onVerifyMessage(message: Message, match: RegExpExecArray | null) {
        const ticket = match ? match[1] : null;
        if (ticket && this.isValid(ticket)) {
            const chatId = message.chat.id;
            this.storage.addChatId(chatId);
            this.storage.removeTicket(ticket);
            this.bot.sendMessage(chatId, "Verfied", { reply_to_message_id: message.message_id })
        }
    }

    private isValid(ticket: string): boolean {
        return this.storage.getTickets().includes(ticket);
    }

    private onDisableMessage(): void {
        this.notifyAll("KornBell is now disabled. No one will receive any more messages until re-enabled using `/enable`.");
        this.disabled = true;
    }

    private onEnableMessage(): void {
        this.disabled = false;
        this.notifyAll("KornBell is enabled again.");
    }

    public sendCatWaiting(): void {
        this.notifyAll("Cat waiting");
    }

    public sendCatGone(): void {
        this.notifyAllSilent("Cat gone");
    }

    public sendCatInside(): void {
        this.notifyAllSilent("Cat is inside");
    }

    private notifyAll(message: string, silent = false) {
        if (!this.disabled) {
            for (const chatId of this.storage.getChatIds()) {
                this.bot.sendMessage(chatId, message, { disable_notification: silent });
            }
        }
    }

    private notifyAllSilent(message: string) {
        this.notifyAll(message, true)
    }
}
