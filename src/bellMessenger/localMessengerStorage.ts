import { LocalStorage } from "node-localstorage";
import { MessengerStorage } from "./messengerStorage";

enum KEY {
    ADMIN_CHAT_ID = "adminChatId",
    TICKETS = "tickets",
    CHATS = "chats"
}

export class LocalMessengerStorage implements MessengerStorage {


    public constructor(private storage: LocalStorage) {
    }

    getAdminChatId(): number {
        const readId = this.storage.getItem(KEY.ADMIN_CHAT_ID);
        const parsedId = parseInt(readId || "-1");
        if (isNaN(parsedId)) {
            return -1;
        } else {
            return parsedId;
        }
    }

    public addTicket(ticket: string): void {
        const tickets = this.getTickets();
        tickets.push(ticket);
        this.writeArray(KEY.TICKETS, tickets);
    }

    private readStringArray(key: KEY): string[] {
        return JSON.parse(this.storage.getItem(key) || "[]");
    }

    private writeArray(key: KEY, values: Array<string | number>): void {
        this.storage.setItem(key, JSON.stringify(values));
    }

    public removeTicket(ticket: string): void {
        let tickets = this.getTickets();
        const position = tickets.findIndex((t) => t === ticket);
        if (position > -1) {
            tickets = tickets.splice(position - 1, 1);
            this.writeArray(KEY.TICKETS, tickets);
        }
    }

    public getTickets(): string[] {
        return this.readStringArray(KEY.TICKETS);
    }

    public addChatId(chatId: number): void {
        const chatIds = this.getChatIds();
        chatIds.push(chatId);
        this.writeArray(KEY.CHATS, chatIds);
    }

    private readIntArray(key: KEY): number[] {
        return JSON.parse(this.storage.getItem(key) || "[]");
    }

    public getChatIds(): number[] {
        return this.readIntArray(KEY.CHATS);
    }
}
