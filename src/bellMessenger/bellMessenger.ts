export interface BellMessenger {
    sendCatWaiting(): void;
    sendCatGone(): void;
    sendCatInside(): void;
}
