export interface MessengerStorage {
    getAdminChatId(): number;

    addTicket(ticket: string): void;
    removeTicket(ticket: string): void;
    getTickets(): string[];

    addChatId(chatId: number): void;
    getChatIds(): number[];
}
