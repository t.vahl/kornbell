import { UltraSonicThresholdEmitter, UltrasonicThresholdListener } from "./ultrasonicThresholdEmitter";

export class CatStateManager implements UltrasonicThresholdListener {
    private listeners: CatStateListener[] = [];
    private catWaitingTimeout: NodeJS.Timeout | null = null;

    public constructor(
        private thresholdEmitter: UltraSonicThresholdEmitter,
        private minimumWaitingMillis: number = 10 * 1000) {
        thresholdEmitter.addListener(this);
    }

    public aboveThreshold() {
        if (this.catWaitingTimeout !== null) {
            clearTimeout(this.catWaitingTimeout);
            this.catWaitingTimeout = null;
        } else {
            this.listeners.forEach(l => l.catGone());
        }
    }

    public belowThreshold() {
        this.catWaitingTimeout = setTimeout(() => {
            this.listeners.forEach(l => l.catWaiting());
            this.catWaitingTimeout = null;
        }, this.minimumWaitingMillis);
    }

    public addListener(listener: CatStateListener) {
        this.listeners.push(listener);
    }
}

export interface CatStateListener {
    catWaiting: () => void;
    catGone: () => void;
}
