import TelegramBot from "node-telegram-bot-api";
import { readFileSync } from "fs";
import { CatStateManager } from "./catStateManager";
import { UltraSonicThresholdEmitter } from "./ultrasonicThresholdEmitter";
import { TelegramBellMessenger } from "./bellMessenger/telegramBellMessenger";
import { LocalMessengerStorage } from "./bellMessenger/localMessengerStorage";
import { LocalStorage } from "node-localstorage";
import { UltrasonicPythonServer } from "./ultrasonicPythonServer";
import { spawn } from "child_process";

const env: {
    token: string;
    minimumWaitingMillis: number;
    echoPin: number;
    triggerPin: number;
    thresholdCms: number;
    sensorIntervalMillis: number;
} = JSON.parse(readFileSync("env.json", { encoding: "utf8" }) || "");
const bot = new TelegramBot(env.token, {
    polling: true
});
const storage = new LocalMessengerStorage(new LocalStorage("messengerStorage"));
const messenger = new TelegramBellMessenger(bot, storage);

const ultrasonicServer = new UltrasonicPythonServer(spawn);
ultrasonicServer.getReady().then(() => {
    const sensor = { measureCm: () => ultrasonicServer.getLast() };
    const emitter = new UltraSonicThresholdEmitter(sensor, env.thresholdCms, env.sensorIntervalMillis);
    const stateManager = new CatStateManager(emitter, env.minimumWaitingMillis);
    stateManager.addListener({
        catWaiting: () => messenger.sendCatWaiting(),
        catGone: () => messenger.sendCatGone()
    });
});
